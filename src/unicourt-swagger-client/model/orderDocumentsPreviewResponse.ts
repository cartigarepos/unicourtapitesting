/**
 * UniCourt - Developer Hub API\'s
 * Unicourt Developer Hub API\'s.
 *
 * The version of the OpenAPI document: v1
 * Contact: support@unicourt.com
 *
 * NOTE: This class is auto generated by OpenAPI Generator (https://openapi-generator.tech).
 * https://openapi-generator.tech
 * Do not edit the class manually.
 */
import { OrderDocumentsPreviewResponseData } from './orderDocumentsPreviewResponseData';


export interface OrderDocumentsPreviewResponse { 
    /**
     * value: false
     */
    error: boolean;
    /**
     * value: OK
     */
    message: string;
    data: OrderDocumentsPreviewResponseData;
}

