/**
 * UniCourt - Developer Hub API\'s
 * Unicourt Developer Hub API\'s.
 *
 * The version of the OpenAPI document: v1
 * Contact: support@unicourt.com
 *
 * NOTE: This class is auto generated by OpenAPI Generator (https://openapi-generator.tech).
 * https://openapi-generator.tech
 * Do not edit the class manually.
 */


export interface SearchRequestQuery { 
    search_terms: Array<string>;
    scope?: SearchRequestQuery.ScopeEnum;
    attribute?: SearchRequestQuery.AttributeEnum;
    proximity?: SearchRequestQuery.ProximityEnum;
    condition?: SearchRequestQuery.ConditionEnum;
}
export namespace SearchRequestQuery {
    export type ScopeEnum = 'Contains Any' | 'Does Not Contain Any' | 'Contains All' | 'Does Not Contain All';
    export const ScopeEnum = {
        ContainsAny: 'Contains Any' as ScopeEnum,
        DoesNotContainAny: 'Does Not Contain Any' as ScopeEnum,
        ContainsAll: 'Contains All' as ScopeEnum,
        DoesNotContainAll: 'Does Not Contain All' as ScopeEnum
    };
    export type AttributeEnum = 'Everything' | 'Party' | 'Case Number' | 'Case Name' | 'Docket' | 'Attorney' | 'Judge' | 'norm_party_id' | 'norm_attorney_id' | 'norm_law_firm_id' | 'norm_judge_id';
    export const AttributeEnum = {
        Everything: 'Everything' as AttributeEnum,
        Party: 'Party' as AttributeEnum,
        CaseNumber: 'Case Number' as AttributeEnum,
        CaseName: 'Case Name' as AttributeEnum,
        Docket: 'Docket' as AttributeEnum,
        Attorney: 'Attorney' as AttributeEnum,
        Judge: 'Judge' as AttributeEnum,
        NormPartyId: 'norm_party_id' as AttributeEnum,
        NormAttorneyId: 'norm_attorney_id' as AttributeEnum,
        NormLawFirmId: 'norm_law_firm_id' as AttributeEnum,
        NormJudgeId: 'norm_judge_id' as AttributeEnum
    };
    export type ProximityEnum = 'Next To Each Other' | 'Anywhere';
    export const ProximityEnum = {
        NextToEachOther: 'Next To Each Other' as ProximityEnum,
        Anywhere: 'Anywhere' as ProximityEnum
    };
    export type ConditionEnum = 'OR' | 'AND';
    export const ConditionEnum = {
        Or: 'OR' as ConditionEnum,
        And: 'AND' as ConditionEnum
    };
}


