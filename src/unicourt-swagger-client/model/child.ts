/**
 * UniCourt - Developer Hub API\'s
 * Unicourt Developer Hub API\'s.
 *
 * The version of the OpenAPI document: v1
 * Contact: support@unicourt.com
 *
 * NOTE: This class is auto generated by OpenAPI Generator (https://openapi-generator.tech).
 * https://openapi-generator.tech
 * Do not edit the class manually.
 */
import { JurisdictionFilterChildren } from './jurisdictionFilterChildren';


export interface Child { 
    field?: string;
    key?: number;
    name?: string;
    order?: number;
    /**
     * list will be having multiple objecs with different values.
     */
    children?: Array<JurisdictionFilterChildren>;
}

