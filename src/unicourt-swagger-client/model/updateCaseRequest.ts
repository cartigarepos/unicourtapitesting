/**
 * UniCourt - Developer Hub API\'s
 * Unicourt Developer Hub API\'s.
 *
 * The version of the OpenAPI document: v1
 * Contact: support@unicourt.com
 *
 * NOTE: This class is auto generated by OpenAPI Generator (https://openapi-generator.tech).
 * https://openapi-generator.tech
 * Do not edit the class manually.
 */
import { AdditionalPage } from './additionalPage';
import { FetchParticipants } from './fetchParticipants';


export interface UpdateCaseRequest { 
    fetch_participants?: FetchParticipants;
    additional_pages?: Array<AdditionalPage>;
}

