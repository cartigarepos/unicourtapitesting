/**
 * UniCourt - Developer Hub API\'s
 * Unicourt Developer Hub API\'s.
 *
 * The version of the OpenAPI document: v1
 * Contact: support@unicourt.com
 *
 * NOTE: This class is auto generated by OpenAPI Generator (https://openapi-generator.tech).
 * https://openapi-generator.tech
 * Do not edit the class manually.
 */


export interface AdditionalPage { 
    page: AdditionalPage.PageEnum;
    fetch_if_older_than: number;
}
export namespace AdditionalPage {
    export type PageEnum = 'ASSOCIATED_CASES' | 'CASE_SUMMARY';
    export const PageEnum = {
        AssociatedCases: 'ASSOCIATED_CASES' as PageEnum,
        CaseSummary: 'CASE_SUMMARY' as PageEnum
    };
}


