/**
 * UniCourt - Developer Hub API\'s
 * Unicourt Developer Hub API\'s.
 *
 * The version of the OpenAPI document: v1
 * Contact: support@unicourt.com
 *
 * NOTE: This class is auto generated by OpenAPI Generator (https://openapi-generator.tech).
 * https://openapi-generator.tech
 * Do not edit the class manually.
 */
import { States } from './states';


export interface PartyPotential { 
    /**
     * Uniquely identifies a potential address for this party.
     */
    party_potential_id: string;
    address: string;
    city: string;
    state: States;
    zipcode: string;
    /**
     * The first date that this entity was seen at this location. Formatted as YYYY-MM-DD
     */
    first_seen_date: string | null;
    /**
     * Signifies if the address location is sourced from the court case. If true then this is the confirmed location for the party.
     */
    court_verified: boolean;
}

