/**
 * UniCourt - Developer Hub API\'s
 * Unicourt Developer Hub API\'s.
 *
 * The version of the OpenAPI document: v1
 * Contact: support@unicourt.com
 *
 * NOTE: This class is auto generated by OpenAPI Generator (https://openapi-generator.tech).
 * https://openapi-generator.tech
 * Do not edit the class manually.
 */
import { EntityType } from './entityType';
import { PartyEntity } from './partyEntity';
import { PartyType } from './partyType';


export interface Party { 
    /**
     * ID for the party in this case. This ID is unique within a case and NOT across cases. If the same party were to appear in another case this ID would be different.
     */
    party_id: string;
    party_types: Array<PartyType>;
    entities: Array<PartyEntity>;
    /**
     * Full name of the party as provided by Court.
     */
    fullname: string;
    prefix: string | null;
    suffix: string | null;
    /**
     * First name of the party. This is normalized by UniCourt.
     */
    firstname: string;
    /**
     * Middle name of the party. This is normalized by UniCourt.
     */
    middlename: string | null;
    /**
     * Last name of the party. This is normalized by UniCourt.
     */
    lastname: string | null;
    /**
     * Normalized ID for Party. Use this ID in Search Cases API with attribute norm_party_id to get all cases involving this party. Use this ID in Norm APIs to get additional details for this entity and relationships with other entities.
     */
    norm_party_id: string | null;
    entity_type: EntityType;
}

