import { NgModule, ModuleWithProviders, SkipSelf, Optional } from '@angular/core';
import { Configuration } from './configuration';
import { HttpClient } from '@angular/common/http';

import { AccountUsageRequestService } from './api/accountUsageRequest.service';
import { AttorneysAPIRequestService } from './api/attorneysAPIRequest.service';
import { CaseAPIRequestService } from './api/caseAPIRequest.service';
import { CaseStatusFiltersRequestService } from './api/caseStatusFiltersRequest.service';
import { CaseTypeFiltersRequestService } from './api/caseTypeFiltersRequest.service';
import { DocketEntriesAPIRequestService } from './api/docketEntriesAPIRequest.service';
import { FetchAllTheBillingCyclesForAnAccountService } from './api/fetchAllTheBillingCyclesForAnAccount.service';
import { FetchInformationForAParticularDocumentOfACaseService } from './api/fetchInformationForAParticularDocumentOfACase.service';
import { FetchInformationForAllDocumentsOfACaseService } from './api/fetchInformationForAllDocumentsOfACase.service';
import { FolderAPIRequestService } from './api/folderAPIRequest.service';
import { InvoicesAPIRequestService } from './api/invoicesAPIRequest.service';
import { JurisdictionFiltersRequestService } from './api/jurisdictionFiltersRequest.service';
import { OrderDocumentConfirmAPIRequestService } from './api/orderDocumentConfirmAPIRequest.service';
import { OrderDocumentDetailsAPIRequestService } from './api/orderDocumentDetailsAPIRequest.service';
import { OrderDocumentPreviewAPIRequestService } from './api/orderDocumentPreviewAPIRequest.service';
import { OrderDocumentStatusAPIRequestService } from './api/orderDocumentStatusAPIRequest.service';
import { PartiesAPIRequestService } from './api/partiesAPIRequest.service';
import { RelatedCasesAPIRequestService } from './api/relatedCasesAPIRequest.service';
import { SavedSearchRequestService } from './api/savedSearchRequest.service';
import { SearchAPIRequestService } from './api/searchAPIRequest.service';
import { StartTrackingCasesAPIRequestService } from './api/startTrackingCasesAPIRequest.service';
import { StopTrackingCasesAPIRequestService } from './api/stopTrackingCasesAPIRequest.service';
import { TheURLToDownloadADocumentService } from './api/theURLToDownloadADocument.service';
import { TheURLToDownloadAInvoiceService } from './api/theURLToDownloadAInvoice.service';
import { TrackedCasesAPIRequestService } from './api/trackedCasesAPIRequest.service';
import { TrackingSchedulesAPIRequestService } from './api/trackingSchedulesAPIRequest.service';
import { UpdateCaseAPIRequestService } from './api/updateCaseAPIRequest.service';

@NgModule({
  imports:      [],
  declarations: [],
  exports:      [],
  providers: []
})
export class ApiModule {
    public static forRoot(configurationFactory: () => Configuration): ModuleWithProviders<ApiModule> {
        return {
            ngModule: ApiModule,
            providers: [ { provide: Configuration, useFactory: configurationFactory } ]
        };
    }

    constructor( @Optional() @SkipSelf() parentModule: ApiModule,
                 @Optional() http: HttpClient) {
        if (parentModule) {
            throw new Error('ApiModule is already loaded. Import in your base AppModule only.');
        }
        if (!http) {
            throw new Error('You need to import the HttpClientModule in your AppModule! \n' +
            'See also https://github.com/angular/angular/issues/20575');
        }
    }
}
