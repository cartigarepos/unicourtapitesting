/**
 * UniCourt - Developer Hub API\'s
 * Unicourt Developer Hub API\'s.
 *
 * The version of the OpenAPI document: v1
 * Contact: support@unicourt.com
 *
 * NOTE: This class is auto generated by OpenAPI Generator (https://openapi-generator.tech).
 * https://openapi-generator.tech
 * Do not edit the class manually.
 */
import { HttpHeaders }                                       from '@angular/common/http';

import { Observable }                                        from 'rxjs';

import { DownloadInvoiceResponse } from '../model/models';
import { ErrorResponse } from '../model/models';


import { Configuration }                                     from '../configuration';



export interface TheURLToDownloadAInvoiceServiceInterface {
    defaultHeaders: HttpHeaders;
    configuration: Configuration;

    /**
     * 
     * The RESTful Download Invoice API, returns a URL to download a invoice for specified Invoice number, which can be obtained in the [Invoices API](/developer-hub/api/account_details/invoices) response. &lt;/br&gt; ***Note:*** The download URL will Expire after 60 minutes.
     * @param invoiceNumber Specify Invoice Number to return document invoice URL, Invoice Number can be obtained as a response of  [Invoices API](/developer-hub/api/account_details/invoices)
     * @param token Token which is generated in UniCourt Account, for authorizing the request.
     */
    downloadInvoice(invoiceNumber: string, token: string, extraHttpRequestParams?: any): Observable<DownloadInvoiceResponse>;

}
