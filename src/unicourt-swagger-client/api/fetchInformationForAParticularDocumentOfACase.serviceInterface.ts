/**
 * UniCourt - Developer Hub API\'s
 * Unicourt Developer Hub API\'s.
 *
 * The version of the OpenAPI document: v1
 * Contact: support@unicourt.com
 *
 * NOTE: This class is auto generated by OpenAPI Generator (https://openapi-generator.tech).
 * https://openapi-generator.tech
 * Do not edit the class manually.
 */
import { HttpHeaders }                                       from '@angular/common/http';

import { Observable }                                        from 'rxjs';

import { ErrorResponse } from '../model/models';
import { SingleDocumentResponse } from '../model/models';


import { Configuration }                                     from '../configuration';



export interface FetchInformationForAParticularDocumentOfACaseServiceInterface {
    defaultHeaders: HttpHeaders;
    configuration: Configuration;

    /**
     * 
     * 
     * @param caseId You can get the Case ID as a response of [Search API](/developer-hub/api/search/query).
     * @param documentId Specify Document ID to return document information, Document ID can be obtained as a response of [Docket Entries API](/developer-hub/api/case_details/docket_entries) or [Document API](/developer-hub/api/case_details/documents)
     * @param token Token which is generated in UniCourt Account, for authorizing the request.
     * @param documentsToken Required to get the documents for a case, which can be obtained in the [Case API](/developer-hub/api/case_details/case) response. This token is valid only for one hour from the time it is returned in the Case API response.
     */
    document(caseId: string, documentId: string, token: string, documentsToken: string, extraHttpRequestParams?: any): Observable<SingleDocumentResponse | ErrorResponse>;

}
