/**
 * UniCourt - Developer Hub API\'s
 * Unicourt Developer Hub API\'s.
 *
 * The version of the OpenAPI document: v1
 * Contact: support@unicourt.com
 *
 * NOTE: This class is auto generated by OpenAPI Generator (https://openapi-generator.tech).
 * https://openapi-generator.tech
 * Do not edit the class manually.
 */
import { HttpHeaders }                                       from '@angular/common/http';

import { Observable }                                        from 'rxjs';

import { AttorneysResponse } from '../model/models';
import { ErrorResponse } from '../model/models';


import { Configuration }                                     from '../configuration';



export interface AttorneysAPIRequestServiceInterface {
    defaultHeaders: HttpHeaders;
    configuration: Configuration;

    /**
     * 
     * The RESTful Attorneys API, returns Attorneys of a case in JSON format (100 attorneys/request) for a Case ID. [Case API](/developer-hub/api/case_details/case) returns upto 10 Attorneys for a case. Call this API to get all the Attorneys for a Case. You can get a Case ID as a response of [Search API](/developer-hub/api/search/query)
     * @param caseId You can get the Case ID as a response of [Search API](/developer-hub/api/search/query).
     * @param token Token which is generated in UniCourt Account, for authorizing the request.
     * @param attorneysToken Required to get the Attorneys for a case, which can be obtained in the [Case API](/developer-hub/api/case_details/case) response. This token is valid only for one hour from the time it is returned in the Case API response.
     * @param pageNumber Pagination number for the party pages. Starting from 1.
     */
    attorneys(caseId: string, token: string, attorneysToken: string, pageNumber: number, extraHttpRequestParams?: any): Observable<AttorneysResponse | ErrorResponse>;

}
